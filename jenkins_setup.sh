#!/bin/bash

#install git, as you always forget and it's needed
#to pull the remote repos
sudo yum install -y git

#get the repo file
wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat-stable/jenkins.repo

#install the repo
rpm --import http://pkg.jenkins-ci.org/redhat-stable/jenkins-ci.org.key

#install and start jenkins
yum install -y jenkins
service jenkins start

