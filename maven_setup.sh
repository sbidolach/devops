#! /bin/bash

# get the zip file from the website 
wget http://apache.mirrors.ionfish.org/maven/maven-3/3.3.3/binaries/apache-maven-3.3.3-bin.tar.gz 

# unzip this to /usr/local 
sudo tar xzf apache-maven-3.3.3-bin.tar.gz -C /usr/local 

# create a symbolic link between the program and an easier to remember directory name  
sudo ln -s /usr/local/apache-maven-3.3.3 /usr/local/maven

# Finally, link between the mvn binary and the /bin folder 
sudo ln -s /usr/local/maven/bin/mvn /bin/mvn

# Java 
sudo yum install –y java-1.7.0-openjdk-devel.x86_64
